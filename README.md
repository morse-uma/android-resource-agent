# Android Resource Agent

This application acts as a device and network monitoring application on Android devices. The application can be used directly from the user interface or remotely, making it ideal for automatic testing.

## Usage

The application can be used on the terminal with the provided user interface, or remotely through adb commands.

### Permissions

In order to register radio information the application requires a set of permissions to be granted. The following actions must be performed before using the application through adb.

When the application is launched it will check if all the required permissions have been granted. If not, the application will close and two dialogs will appear, requesting the user to grant this permissions.

Select “Allow” and/or “Allow all the time”. Please note that the application will not record any location information (or perform calls). However, since radio parameters such as cell ID can be used for obtaining the location of the device Android must request these permissions.

Once allowed, the application can be re-opened or used through adb.

### Adb commands

The application can be used remotely sending intents to the service via the startservice command of adb:

`adb shell am startservice -n com.uma.resourceAgent/.ResourceAgentService`

The application accepts the following intents (-a):

`com.uma.resourceAgent.START`

`com.uma.resourceAgent.STOP`


#### Examples

`adb shell am startservice –n com.uma.resourceAgent/.ResourceAgentService –a com.uma.resourceAgent.START`

`adb shell am startservice -n com.uma.resourceAgent/.ResourceAgentService -a com.uma.resourceAgent.STOP`


### Device Interface

The application can be used through the device interface. The application is divided into two parts. The first one monitors the network interface: Operator, Network, Cell ID, LAC, RSSI, PSC, RSRP, SNR, CQI and RSRQ. The second one is used for monitoring the device information: CPU usage; Ram used and available; packets received and transmitted; and bytes transmitted and received. 

The network section is updated with every network change and the device information will be updating one the user presses the Start button and it will take between one and three seconds to update, depending on the device. 

Once the device monitoring is initiated, the Log will be updated during the execution.

## Authors

* Bruno Garcia Garcia
* Gonzalo Chica Morales

## License

Copyright 2020 MORSE Research Group - University of Malaga

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
