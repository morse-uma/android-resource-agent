/**
 * Developed by the University of Málaga
 *
 * Gonzalo Chica Morales
 */

package com.uma.resourceAgent;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthLte;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.uma.enums.NetworkType;
import com.uma.enums.Signal;
import com.uma.signals.ISignal;
import com.uma.util.SignalArrayWrapper;
import com.uma.util.SignalMapWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceAgentActivity extends AppCompatActivity {
    private static InterfaceIds gui;
    TextView operator, network, cellID, lacID, rssi, psc, rsrp, snr, cqi, rsrq;

    private LogReceiver logReceiver;
    private StatusReceiver statusReceiver;
    private static String LOG = "resourceAgent." + ResourceAgentActivity.class.getSimpleName();

    private String  operatorName;
    public static String operatorValue, networkValue, cellIDValue, lacIDValue, rssiValue;
    public static String pscValue, rsrpValue, snrValue, cqiValue, rsrqValue;
    TelephonyManager mTelephMgr;
    MyPhoneStateListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resourceagent);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setTitle(R.string.app_name);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        gui = new InterfaceIds();
        gui.Log = findViewById(R.id.logTextView);
        gui.Scroll = findViewById(R.id.logScroll);
        gui.StartStopButton = findViewById(R.id.startStopButton);
        gui.ClearLogButton = findViewById(R.id.clearLogButton);
        operator =  findViewById(R.id.operator);
        network =  findViewById(R.id.network);
        cellID =  findViewById(R.id.cellID);
        lacID =  findViewById(R.id.lacID);
        rssi =  findViewById(R.id.rssi);
        psc =  findViewById(R.id.psc);
        rsrp =  findViewById(R.id.rsrp);
        snr =  findViewById(R.id.snr);
        cqi =  findViewById(R.id.cqi);
        rsrq =  findViewById(R.id.rsrq);

        Log.d(LOG, "Checking permissions");

        boolean granted = checkPermissions();

        if (granted) {
            Log.d(LOG, "Permission granted");

            mTelephMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mListener = new MyPhoneStateListener();
            mTelephMgr.listen(mListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS | PhoneStateListener.LISTEN_CELL_LOCATION | PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
        } else {
            finish();
        }

        if (savedInstanceState != null) {
            gui.Log.setText(savedInstanceState.getString("Log"));
        } else {
            updateStatus();

            int sdk = Build.VERSION.SDK_INT;
            String name = Build.VERSION.RELEASE;
            String api = (sdk < Build.VERSION_CODES.Q) ? "legacy" : "Android Q";

            gui.Log.append("SDK version " + sdk + "(Android " + name + "). Using " + api + " API.\n");
            gui.Log.append("Initialization completed.\n");
        }
    }

    private boolean checkPermissions() {
        Context context = getApplicationContext();
        String[] permissions;

        if (Build.VERSION.SDK_INT < 29) {
            permissions = new String[] {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE };
        } else {
            permissions = new String[] {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                    Manifest.permission.READ_PHONE_STATE };
        }

        boolean passed = true;

        for (String permission : permissions) {
            passed = passed && checkOnePermission(context, permission);
        }

        if (!passed) {
            ActivityCompat.requestPermissions(this, permissions, 100);
        }

        return passed;
    }

    private boolean checkOnePermission(Context context, String permission) {
        boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d(LOG, permission + " granted: " + granted);
        return granted;
    }

    @Override
    public void onStart() {
        super.onStart();

        operatorName=mTelephMgr.getNetworkOperatorName();
        if (operatorName!=null && !operatorName.equals("")){
            operator.setText(operatorName);
            operatorValue = operatorName;
        } else {
            operator.setText("Not Available");
            operatorValue = "n/a";
        }

        network.setText(formatNetworkType(mTelephMgr.getNetworkType()));
        networkValue = formatNetworkType(mTelephMgr.getNetworkType());
    }

    @Override
    protected void onResume() {
        super.onResume();

        statusReceiver = new StatusReceiver();
        IntentFilter filter = new IntentFilter(ResourceAgentService.UPDATE_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(statusReceiver, filter);

        logReceiver = new LogReceiver();
        filter = new IntentFilter(ResourceAgentService.LOG_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(logReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(logReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("Log", gui.Log.getText().toString());
    }

    private class InterfaceIds {
        TextView Log;
        ScrollView Scroll;
        Button StartStopButton;
        Button ClearLogButton;
    }

    private class StatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateStatus();
        }
    }

    private void updateStatus() {
        if (ResourceAgentService.active()) {
            gui.StartStopButton.setText("Stop");
            gui.StartStopButton.setBackgroundColor(Color.parseColor("#b30000"));
        } else {
            gui.StartStopButton.setText("Start");
            gui.StartStopButton.setBackgroundColor(Color.parseColor("#009933"));
        }
    }

    public void startStopButtonClicked(View view) {
        boolean active = ResourceAgentService.active();
        Intent intent = new Intent(this, ResourceAgentService.class);

        if (active) {
            intent.setAction(ResourceAgentService.STOP_ACTION);
        } else {
            intent.setAction(ResourceAgentService.START_ACTION);
        }
        startService(intent);    }

    public void clearLogButtonClicked(View view) {
        gui.Log.setText("");
    }

    // Utility methods for sending broadcast messages to the application.
    public static void broadcast(LocalBroadcastManager manager, String message) {
        Intent logIntent = new Intent(ResourceAgentService.LOG_BROADCAST_ACTION);
        logIntent.putExtra(ResourceAgentService.LOG_STRING, message + "\n");
        manager.sendBroadcast(logIntent);
    }

    public static void notifyStatusChange(LocalBroadcastManager manager) {
        manager.sendBroadcast(new Intent(ResourceAgentService.UPDATE_BROADCAST_ACTION));
    }

    private class LogReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra(ResourceAgentService.LOG_STRING);
            if (message != null) {
                gui.Log.append(message);
            }
        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {
        // Get the Signal strength from the provider, each time there is an update
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength){
            super.onSignalStrengthsChanged(signalStrength);

            // Defaults
            rssi.setText("Unavailable");
            rsrp.setText("Unavailable");
            snr.setText("Unavailable");
            cqi.setText("Unavailable");
            rsrq.setText("Unavailable");


            rssiValue = rsrpValue = snrValue = cqiValue = rsrqValue = "Unavailable";

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q){
                legacySignalStrenghtsHandler(signalStrength);
            } else {
                nrSignalStrengthHandler(signalStrength);
            }
        }

        private void legacySignalStrenghtsHandler(SignalStrength signalStrength) {
            SignalMapWrapper signalMapWrapper = new SignalMapWrapper(new SignalArrayWrapper(signalStrength).getFilteredArray(), mTelephMgr);
            ISignal signal;

            if (signalMapWrapper.hasData()) {
                Map<NetworkType, ISignal> networkSignals = signalMapWrapper.getNetworkMap();
                int networkType = mTelephMgr.getNetworkType();

                switch (networkType) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_HSPAP:
                        signal = networkSignals.get(NetworkType.GSM);
                        rssiValue = signal.getSignalString(Signal.GSM_RSSI);
                        rssi.setText(rssiValue);
                        break;

                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        signal = networkSignals.get(NetworkType.CDMA);
                        rssiValue = signal.getSignalString(Signal.CDMA_RSSI);
                        rssi.setText(rssiValue);
                        break;

                    case TelephonyManager.NETWORK_TYPE_LTE:
                        signal = networkSignals.get(NetworkType.LTE);
                        rssiValue = signal.getSignalString(Signal.LTE_RSSI);
                        rsrpValue = signal.getSignalString(Signal.LTE_RSRP);
                        snrValue = signal.getSignalString(Signal.LTE_SNR);
                        cqiValue = signal.getSignalString(Signal.LTE_CQI);
                        rsrqValue = signal.getSignalString(Signal.LTE_RSRQ);

                        rssi.setText(rssiValue);
                        rsrp.setText(rsrpValue);
                        snr.setText(snrValue);
                        cqi.setText(cqiValue);
                        rsrq.setText(rsrqValue);
                        break;

                    default:
                        break;
                }
            }
        }

        @TargetApi(Build.VERSION_CODES.Q)
        @RequiresApi(api = Build.VERSION_CODES.O)
        private void nrSignalStrengthHandler(SignalStrength signalStrength){
            List<CellSignalStrength> strengths =  signalStrength.getCellSignalStrengths();

            for (int i = 0; i < strengths.size(); i++)
            {
                CellSignalStrength strength = strengths.get(i);

                if (strength instanceof CellSignalStrengthLte) {
                    CellSignalStrengthLte lte = (CellSignalStrengthLte)strength;

                    rssiValue = valueOrUnavailable(String.valueOf(lte.getRssi()));
                    rsrpValue = valueOrUnavailable(String.valueOf(lte.getRsrp()));
                    snrValue = valueOrUnavailable(String.valueOf((double)lte.getRssnr()/10.0));
                    cqiValue = valueOrUnavailable(String.valueOf(lte.getCqi()));
                    rsrqValue = valueOrUnavailable(String.valueOf(lte.getRsrq()));

                    rssi.setText(rssiValue);
                    rsrp.setText(rsrpValue);
                    snr.setText(snrValue);
                    cqi.setText(cqiValue);
                    rsrq.setText(rsrqValue);
                }
            }
        }

        private String valueOrUnavailable(String value){
            String unavailable = String.valueOf(CellInfo.UNAVAILABLE);
            if (value.equals(unavailable) || value.length() > 5){
                return "Unavailable";
            } else {
                return value;
            }
        }

        @Override
        public void onCellLocationChanged(CellLocation location) {
            // Get the Cell location each time there is an update
            super.onCellLocationChanged(location);
            GsmCellLocation loc= (GsmCellLocation) location;

            psc.setText(String.valueOf(loc.getPsc()));
            pscValue = String.valueOf(loc.getPsc());

            if (loc.getCid()!=-1){
                cellID.setText(String.valueOf(loc.getCid()));
                cellIDValue = String.valueOf(loc.getCid());
            } else {
                cellID.setText("Unavailable");
                cellIDValue = "Unavailable";
            }
            if (loc.getLac()!=-1){
                lacID.setText(String.valueOf(loc.getLac()));
                lacIDValue = String.valueOf(loc.getLac());
            } else {
                lacID.setText("Unavailable");
                lacIDValue = "Unavailable";
            }

        }

        @Override
        public void onDataConnectionStateChanged(int state,int networkType) {
            super.onDataConnectionStateChanged(state, networkType);
            operatorName=mTelephMgr.getNetworkOperatorName();
            if (operatorName!=null && !operatorName.equals("")){
                operator.setText(operatorName);
                operatorValue = operatorName;
            } else {
                operator.setText("Unavailable");
                operatorValue = "Unavailable";
            }
            network.setText(formatNetworkType(networkType));
            networkValue = formatNetworkType(networkType);
        }
    }

    private String formatNetworkType (int networkType){
        //converts Network Type Id to text
        switch (networkType) {
            case 1: return "GPRS";
            case 2: return "EDGE";
            case 3: return "UMTS";
            case 4: return "CDMA";
            case 5: return "EVDO_0";
            case 6: return "EVDO_A";
            case 7: return "1xRTT";
            case 8: return "HSDPA";
            case 9: return "HSUPA";
            case 10: return "HSPA";
            case 11: return "iDEN";
            case 12: return "EVDO_B";
            case 13: return "LTE";
            case 14: return "eHRPD";
            default : return "Unknown";
        }
    }

    public static Map<String, String> NetworkValues() {
        Map<String, String> network_values = new HashMap<>();
        network_values.put("operatorValue", operatorValue);
        network_values.put("networkValue", networkValue);
        network_values.put("cellIDValue", cellIDValue);
        network_values.put("lacIDValue", lacIDValue);
        network_values.put("rssiValue", rssiValue);
        network_values.put("pscValue", pscValue);
        network_values.put("rsrpValue", rsrpValue);
        network_values.put("snrValue", snrValue);
        network_values.put("cqiValue", cqiValue);
        network_values.put("rsrqValue", rsrqValue);

        return network_values;
    }

}
