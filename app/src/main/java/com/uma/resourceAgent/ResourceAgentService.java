/**
 * Developed by the University of Málaga
 *
 * Gonzalo Chica Morales
 */

package com.uma.resourceAgent;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

public class ResourceAgentService extends Service {
    public final static String START_ACTION = "com.uma.resourceAgent.START";
    public final static String STOP_ACTION = "com.uma.resourceAgent.STOP";
    public final static String UPDATE_BROADCAST_ACTION = "com.uma.resourceAgent.UPDATEACTION";
    public final static String LOG_BROADCAST_ACTION = "com.uma.resourceAgent.LOGACTION";
    public final static String LOG_STRING = "com.uma.resourceAgent.LOGSTRING";

    private static String LOG = "resourceAgent." +  ResourceAgentService.class.getSimpleName();
    private static ResourceAgentTask resourceAgentTask = null;

    private static int taskId = 0;

    public ResourceAgentService() {
    }

    public static void finished() {
        Log.i(LOG, "Resource Agent has finished.");
        resourceAgentTask = null;
    }

    public static boolean active() {
        return resourceAgentTask != null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        taskId++;

        if (action == null) {
            Log.w(LOG, "Received intent with no action. Call ignored.");
        } else {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            String output = "\nError while processing received service request.";

            String MESSAGE = "resourceAgent.Message";
            switch (action) {
                case START_ACTION:
                    Log.i(LOG, "START_ACTION");
                    if (resourceAgentTask != null) {
                        output = "\nResource Agent is already started. Call ignored.";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        output = String.format("\nStarting Resource Agent.");
                        ResourceAgentActivity.notifyStatusChange(broadcastManager);
                        resourceAgentTask = new ResourceAgentTask(this, taskId);
                        resourceAgentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    break;
                case STOP_ACTION:
                    Log.i(LOG, "STOP_ACTION");
                    if (resourceAgentTask == null) {
                        output = "\nResource Agent task is not running. Call ignored.";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        output = "\nStopping Resource Agent.";
                        ResourceAgentActivity.notifyStatusChange(broadcastManager);
                        resourceAgentTask.cancel(true);
                        resourceAgentTask = null;
                    }
                    break;
            }
            ResourceAgentActivity.broadcast(broadcastManager, output);
        }
        return Service.START_NOT_STICKY;
    }
}
