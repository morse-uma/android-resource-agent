/**
 * Developed by the University of Málaga
 *
 * Gonzalo Chica Morales
 */

package com.uma.resourceAgent;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.net.TrafficStats;

public class ResourceAgentTask extends AsyncTask<String, String, Integer> {
    private static String LOG = "resourceAgent." + ResourceAgentTask.class.getSimpleName();
    private static Pattern top_old_pattern = Pattern.compile("User (\\d+)%, System (\\d+)%, IOW (\\d+)%, IRQ (\\d+)%"); //TOP old version
    private static Pattern top_new_pattern = Pattern.compile("(\\d+)%idle"); //TOP new version
    private static SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
    private Context context;
    private String taskId;
    private long previousPacketsReceived = -1;
    private long previousPacketsTransmitted = -1;
    private long previousBytesReceived = -1;
    private long previousBytesTransmitted = -1;
    private OutputStreamWriter file;
    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");

    ResourceAgentTask(Context c, int id) {
        context = c;
        taskId = "resourceAgent<" + id + ">";
        fileOpen();
    }

    private void fileOpen() {
        File externalStorage = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS);
        String folderpath = externalStorage.getAbsolutePath() + "/resourceAgent/";
        String filepath = folderpath + format.format(Calendar.getInstance().getTime()) + ".txt";

        try {
            File folderHandle = new File(folderpath);
            if (!folderHandle.exists()) {
                Log.i(LOG, "Creating app folder. Success: " + folderHandle.mkdirs());
            }

            File fileHandle = new File(filepath);
            if (!fileHandle.createNewFile()) {
                throw new IOException("File " + filepath + " already exists");
            }

            file = new OutputStreamWriter(new FileOutputStream(fileHandle));
        }
        catch (IOException e) {
            Log.e(LOG, "File open '" + filepath + "' failed: " + e.toString());
        }
    }

    private void fileClose() {
        if (file != null) {
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                Log.e(LOG, "File close failed: " + e.toString());
            }
        }
    }

    protected Integer doInBackground(String... params) {

        boolean firstTime = true;
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
        ResourceAgentActivity.broadcast(broadcastManager, "\nResource Agent task has started.");
        Log.i(LOG, "Starting Resource Agent task: " + taskId);

        double cpuUsage;

        while (!isCancelled()) {
            long timestamp = System.currentTimeMillis();
            Log.d(LOG, "Time: " + timestamp);

            cpuUsage = getCPUUsage();
            Log.d(LOG, "CPU usage: " + cpuUsage + "%");

            List<Long> ramMemorySize = getRamMemorySize();
            long availableRam = ramMemorySize.get(0);
            long usedRam = ramMemorySize.get(1);
            Log.d(LOG, "Available Ram: " + availableRam + " MBs");
            Log.d(LOG, "Used Ram: " + usedRam + " MBs");

            List<Long> packetsTxRx = getPacketsTxRx();
            long packetsTransmitted = packetsTxRx.get(0);
            long packetsReceived = packetsTxRx.get(1);
            Log.d(LOG, "Packets Transmitted: " + packetsTransmitted);
            Log.d(LOG, "Packets Received: " + packetsReceived);

            List<Long> bytesTxRx = getBytesTxRx();
            long bytesTransmitted = bytesTxRx.get(0);
            long bytesReceived = bytesTxRx.get(1);
            Log.d(LOG, "Bytes Transmitted: " + bytesTransmitted);
            Log.d(LOG, "Bytes Received: " + bytesReceived);

            long elapsed_time = System.currentTimeMillis() - timestamp;
            if (elapsed_time < 1000) {
                try {
                    Thread.sleep(1000-elapsed_time);
                } catch (InterruptedException e) {
                    Log.d(LOG, "Interrupted while waiting for Resource Agent process output: " + e.getMessage());
                }
            }

            if(firstTime) {
                firstTime = false;
            } else {
                Date currentDate = new Date(timestamp);
                ResourceAgentActivity.broadcast(broadcastManager, "\n" +
                        "\nTIME    | "+ df.format(currentDate) +
                        "\nCPU     | Used: " + cpuUsage + "%" +
                        "\nRAM     | Used: " + String.format("%-5d", usedRam)         + "MB | Avail: " + String.format("%-5d", availableRam) + "MB" +
                        "\nPACKETS | Rcvd: " + String.format("%-7d", packetsReceived) +   " | Trans: " + String.format("%-5d", packetsTransmitted) +
                        "\nBYTES   | Rcvd: " + String.format("%-7d", bytesReceived)   +   " | Trans: " + String.format("%-5d", bytesTransmitted));

                Map<String, String> network_values = ResourceAgentActivity.NetworkValues();

                String message = "<<< Elapsed time " + (double)elapsed_time/1000 + " sec ; Timestamp "+ timestamp +
                        " ; CPU usage " + cpuUsage + "% ; Ram used " + usedRam + "MBs ; Available Ram " + availableRam + "MBs" +
                        " ; Packets Received " + packetsReceived + " ; Packets Transmitted " + packetsTransmitted +
                        " ; Bytes Received " + bytesReceived + " ; Bytes Transmitted " + bytesTransmitted +
                        " ; Operator "+ network_values.get("operatorValue") + " ; Network " + network_values.get("networkValue") +
                        " ; Cell ID " + network_values.get("cellIDValue") + " ; LAC " + network_values.get("lacIDValue") +
                        " ; RSSI " + network_values.get("rssiValue") + " ; PSC " + network_values.get("pscValue") +
                        " ; RSRP " + network_values.get("rsrpValue") + " ; SNR " + network_values.get("snrValue") +
                        " ; CQI " + network_values.get("cqiValue") + " ; RSRQ " + network_values.get("rsrqValue") + " >>>";

                Log.i(LOG, message);

                try {
                    file.write(message + '\n');
                } catch (IOException e) {
                    Log.e(LOG, "Could not write to file: " + e.toString());
                }
            }
        }

        ResourceAgentActivity.broadcast(broadcastManager, "\nResource Agent task has finished.");
        ResourceAgentService.finished();
        ResourceAgentActivity.notifyStatusChange(broadcastManager);
        fileClose();

        return 0;
    }

    private double getCPUUsage() {
        Process process;
        int idle = 0;
        boolean found = false;
        double usage = 0;
        double cpuCount = Runtime.getRuntime().availableProcessors();

        try {
            process = Runtime.getRuntime().exec(new String[]{"sh", "-c", "top -n 1"});
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String buffer = bufferedReader.readLine();
            String[] lines = (buffer != null) ?
                    buffer.split("(\\r\\n|\\r|\\n)") : new String[] {};
            for(int i = 0; i< lines.length && !found; i++){
                String line = lines[i];
                while (line  != null && !line.toUpperCase().contains("IOW")) {
                    line = bufferedReader.readLine();
                }
                if (line != null){
                    Matcher m = top_old_pattern.matcher(line);
                    if(m.find()){
                        MatchResult mr=m.toMatchResult();
                        for(int j=1;j<=4;j++){
                            usage+=Integer.valueOf(mr.group(j));
                        }
                        found = true;
                    }else{
                        Matcher m2= top_new_pattern.matcher(line);
                        if(m2.find()) {
                            MatchResult mr2 = m2.toMatchResult();
                            idle=Integer.valueOf(mr2.group(1));
                            usage = 100 - (idle/cpuCount);
                            found = true;
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return usage;
    }

    private List<Long> getRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager)  context.getSystemService(Activity.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);

        long availableRam = mi.availMem / 1048576L;
        long usedRam = (mi.totalMem / 1048576L) - availableRam;

        List<Long> RamMemorySize = new ArrayList<>();
        RamMemorySize.add(availableRam);
        RamMemorySize.add(usedRam);

        return RamMemorySize;
    }

    private List<Long> getPacketsTxRx() {
        long totalPacketsTransmitted = TrafficStats.getTotalTxPackets();
        long totalPacketsReceived = TrafficStats.getTotalRxPackets();

        List<Long> packetsTxRx = new ArrayList<>();
        packetsTxRx.add(totalPacketsTransmitted - previousPacketsTransmitted);
        packetsTxRx.add(totalPacketsReceived - previousPacketsReceived);

        previousPacketsTransmitted = totalPacketsTransmitted;
        previousPacketsReceived = totalPacketsReceived;

        return packetsTxRx;
    }

    private List<Long> getBytesTxRx() {
        long totalBytesTransmitted = TrafficStats.getTotalTxBytes();
        long totalBytesReceived = TrafficStats.getTotalRxBytes();

        if (totalBytesReceived == TrafficStats.UNSUPPORTED || totalBytesTransmitted == TrafficStats.UNSUPPORTED) {
            Log.e(LOG, "The use of TrafficStats is not supported on this device.");
        }

        List<Long> bytesTxRx = new ArrayList<>();
        bytesTxRx.add(totalBytesTransmitted - previousBytesTransmitted);
        bytesTxRx.add(totalBytesReceived - previousBytesReceived);

        previousBytesTransmitted = totalBytesTransmitted;
        previousBytesReceived = totalBytesReceived;

        return bytesTxRx;
    }

}
